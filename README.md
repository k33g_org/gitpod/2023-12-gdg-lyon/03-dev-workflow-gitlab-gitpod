# 03-dev-workflow-gitlab-gitpod

- [🍊 Open it with Gitpod](https://gitpod.io/#https://gitlab.com/k33g_org/gitpod/2023-12-gdg-lyon/03-dev-workflow-gitlab-gitpod)
- [🐳 Open it with Docker Dev Environment](https://open.docker.com/dashboard/dev-envs?url=https://gitlab.com/k33g_org/gitpod/2023-12-gdg-lyon/03-dev-workflow-gitlab-gitpod/tree/main)


